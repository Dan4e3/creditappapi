using AutoMapper;
using CreditApp.Abstractions.Repositories.Interfaces;
using CreditApp.Controllers;
using CreditApp.Models;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CreditApp.Tests
{
    public class ApplicationControllerTests
    {
        [Fact]
        public async void GetAllReturnsAllCreditAppliancesList()
        {
            var repoMock = new Mock<IDataRepository<CreditAppliance>>();
            var mapperMock = new Mock<IMapper>();
            var loggerMock = new Mock<ILogger<ApplicationController>>();
            repoMock.Setup(rep => rep.GetAllAsync()).Returns(Task.FromResult(GetTestCreditAppliances()));
            var controller = new ApplicationController(repoMock.Object, mapperMock.Object, loggerMock.Object);

            var result = await controller.GetAll();

            Assert.Equal(new List<CreditAppliance>(result.Value).Count, GetTestCreditAppliances().Count);
        }

        private List<CreditAppliance> GetTestCreditAppliances()
        {
            return new List<CreditAppliance>()
            {
                new CreditAppliance() { }, new CreditAppliance{ }, new CreditAppliance{ }
            };
        }
    }
}
