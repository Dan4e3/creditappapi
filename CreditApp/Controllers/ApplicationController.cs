﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using CreditApp.Abstractions.Contexts;
using CreditApp.Abstractions.Repositories;
using CreditApp.Abstractions.Repositories.Interfaces;
using CreditApp.DataTransferObjects;
using CreditApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CreditApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IDataRepository<CreditAppliance> _repository;
        private readonly IMapper _mapper;
        private readonly ILogger<ApplicationController> _logger;

        public ApplicationController(IDataRepository<CreditAppliance> repository, IMapper mapper, 
            ILogger<ApplicationController> logger)
        {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<CreditAppliance>> Create(CreditApplianceDTO creditApplianceDTO)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid model input");
                return BadRequest(ModelState);
            }

            CreditAppliance mappedEntity = _mapper.Map<CreditAppliance>(creditApplianceDTO);
            CreditAppliance addedEntity = await _repository.AddAsync(mappedEntity);
            SetScoringAsync(addedEntity.Id);

            _logger.LogInformation("New credit appliance was added");

            return Ok(new
            {
                id = addedEntity.Id,
                addedEntity.ApplicationNum
            });
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CreditAppliance>>> GetAll()
        {
            return await _repository.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CreditApplianceStatusDTO>> Status(int id)
        {
            return await _repository.GetWithDTOAsync<CreditApplianceStatusDTO>(id);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<CreditAppliance>> Delete(int id)
        {
            return await _repository.DeleteAsync(id);
        }
        
        private async void SetScoringAsync(int entityId)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(@"https://localhost:44398/api/scoring/evaluate");
                if (response.IsSuccessStatusCode)
                {
                    var ObjResponse = response.Content.ReadAsStringAsync().Result;
                    dynamic json = JsonConvert.DeserializeObject(ObjResponse);
                    var rep = new CreditApplianceRepository(CreditApplianceContext.CreateDefaultContext(), _mapper);
                    CreditAppliance entity = await rep.GetAsync(entityId);
                    entity.ScoringStatus = json.scoringStatus;
                    entity.ScoringDate = DateTime.Now;
                    if (entity.ScoringStatus)
                        _logger.LogInformation($"Credit appliance #{entity.ApplicationNum} approved!");
                    else
                        _logger.LogInformation($"Credit appliance #{entity.ApplicationNum} disapproved!");
                    await rep.UpdateAsync(entity);
                }
            }
        }
    }
}