﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CreditApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ScoringController : ControllerBase
    {
        private readonly ILogger<ApplicationController> _logger;

        public ScoringController(ILogger<ApplicationController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<object>> Evaluate()
        {
            _logger.LogInformation("Scoring is in process...");
            return Ok(new {
                ScoringStatus = await ScoringMockAsync.ScoreWithTimeoutAsync()
            });
        }
    }

    public static class ScoringMockAsync
    {
        static Random _rand = new Random();

        public async static Task<bool> ScoreWithTimeoutAsync(int msTimeout = 5000)
        {
            bool scoringResult = _rand.Next(0, 1) == 0 ? false : true;
            await Task.Delay(msTimeout);
            return scoringResult;
        }
    }
}