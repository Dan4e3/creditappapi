﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.Models
{
    public class RequestedCredit
    {
        public int Id { get; set; }

        [Required]
        public int CreditType { get; set; }

        [Required]
        public decimal RequestedAmount { get; set; }

        [Required]
        public string RequestedCurrency { get; set; }

        [Required]
        public decimal AnnualSalary { get; set; }

        [Required]
        public decimal MonthlySalary { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string Comment { get; set; }

        public int CreditApplianceId { get; set; }
        public CreditAppliance CreditAppliance { get; set; }
    }
}
