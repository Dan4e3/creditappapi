﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.Models
{
    public class Applicant
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string MiddleName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime DateBirth { get; set; }

        [Required]
        public string CityBirth { get; set; }

        [Required]
        public string AddressBirth { get; set; }

        [Required]
        public string AddressCurrent { get; set; }

        [Required]
        public string INN { get; set; }

        [Required]
        public string SNILS { get; set; }

        [Required]
        public string PassportNum { get; set; }

        public int CreditApplianceId { get; set; }
        public CreditAppliance CreditAppliance { get; set; }
    }
}
