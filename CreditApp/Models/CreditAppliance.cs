﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CreditApp.Models
{
    public class CreditAppliance
    {
        public int Id { get; set; }
        [Required]
        public string ApplicationNum { get; set; }

        [Required]
        public DateTime ApplicationDate { get; set; }

        [Required]
        public string BranchBank { get; set; }

        [Required]
        public string BranchBankAddr { get; set; }

        [Required]
        public int CreditManagerId { get; set; }

        [Required] 
        public Applicant Applicant { get; set; }

        [Required]
        public RequestedCredit RequestedCredit { get; set; }
        public bool ScoringStatus { get; set; }
        public DateTime ScoringDate { get; set; }
    }
}
