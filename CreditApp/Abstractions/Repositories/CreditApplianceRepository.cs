﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CreditApp.Abstractions.Contexts;
using CreditApp.Abstractions.Repositories.Interfaces;
using CreditApp.DataTransferObjects;
using CreditApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.Abstractions.Repositories
{
    public class CreditApplianceRepository : IDataRepository<CreditAppliance>
    {
        CreditApplianceContext _context;
        IMapper _mapper;
        public CreditApplianceRepository(CreditApplianceContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CreditAppliance> AddAsync(CreditAppliance entity)
        {
            var res = await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return res.Entity;
        }

        public async Task<CreditAppliance> DeleteAsync(int id)
        {
            CreditAppliance res = await _context.CreditAppliances.FirstOrDefaultAsync(x => x.Id == id);
            if (res != null)
            {
                _context.CreditAppliances.Remove(res);
                await _context.SaveChangesAsync();
            }

            return res;
        }

        public async Task<CreditAppliance> GetAsync(int id)
        {
            return await _context.CreditAppliances.FindAsync(id);
        }

        public async Task<List<CreditAppliance>> GetAllAsync()
        {
            return await _context.CreditAppliances
                .Include(x => x.Applicant)
                .Include(x => x.RequestedCredit)
                .ToListAsync();
        }

        public async Task<U> GetWithDTOAsync<U>(int id)
        {
            return await _mapper.ProjectTo<U>(_context.CreditAppliances.Where(x => x.Id == id)).FirstOrDefaultAsync();
        }

        public async Task<CreditAppliance> UpdateAsync(CreditAppliance entity)
        {
            //CreditAppliance entityToUpdate = await _context.CreditAppliances.FindAsync(entity.Id);
            //if (entityToUpdate == null)
            //    return null;

            //entityToUpdate.ScoringStatus = entity.ScoringStatus;
            //entityToUpdate.ScoringDate = entity.ScoringDate;
            _context.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
