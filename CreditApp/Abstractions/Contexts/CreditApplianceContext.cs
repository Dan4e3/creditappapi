﻿using CreditApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.Abstractions.Contexts
{
    public class CreditApplianceContext: DbContext
    {
        public CreditApplianceContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public static CreditApplianceContext CreateDefaultContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CreditApplianceContext>();
            optionsBuilder.UseSqlServer("Server=PREDATOR-HELIOS;Database=CreditApp;User ID = CreditUser; pwd=123");
            return new CreditApplianceContext(optionsBuilder.Options);
        }

        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<CreditAppliance> CreditAppliances { get; set; }
        public DbSet<RequestedCredit> RequestedCredit { get; set; }
    }
}
