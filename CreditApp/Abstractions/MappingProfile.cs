﻿using AutoMapper;
using CreditApp.DataTransferObjects;
using CreditApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.Abstractions
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<CreditAppliance, CreditApplianceDTO>();
            CreateMap<CreditApplianceDTO, CreditAppliance>();
            CreateMap<CreditAppliance, CreditApplianceStatusDTO>();
            CreateMap<Applicant, ApplicantDTO>();
            CreateMap<RequestedCredit, RequestedCreditDTO>();
        }
    }
}
