﻿using CreditApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.DataTransferObjects
{
    public class CreditApplianceDTO
    {
        public int Id { get; set; }

        [Required]
        public string ApplicationNum { get; set; }

        [Required]
        public DateTime ApplicationDate { get; set; }

        [Required]
        public string BranchBank { get; set; }

        [Required]
        public string BranchBankAddr { get; set; }

        [Required]
        public int CreditManagerId { get; set; }

        [Required]
        public Applicant Applicant { get; set; }

        [Required]
        public RequestedCredit RequestedCredit { get; set; }
    }
}
