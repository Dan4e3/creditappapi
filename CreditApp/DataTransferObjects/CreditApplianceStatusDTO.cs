﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditApp.DataTransferObjects
{
    public class CreditApplianceStatusDTO
    {
        public int Id { get; set; }
        public string ApplicationNum { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string BranchBank { get; set; }
        public string BranchBankAddr { get; set; }
        public int CreditManagerId { get; set; }
        public ApplicantDTO Applicant { get; set; }
        public RequestedCreditDTO RequestedCredit { get; set; }
        public bool ScoringStatus { get; set; }
        public DateTime ScoringDate { get; set; }
    }
}
